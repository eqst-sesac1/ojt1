<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="user.UserDAO" %>
<%@ page import="java.io.PrintWriter" %>
<% request.setCharacterEncoding("UTF-8"); %>
<jsp:useBean id="user" class="user.User" scope="page" />
<jsp:setProperty name="user" property="userPassword" />
<jsp:setProperty name="user" property="userName" />
<jsp:setProperty name="user" property="userGender" />
<jsp:setProperty name="user" property="userEmail" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		//현재 세션 상태 확인
		boolean delete = false;
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");  //세션의 userid 확인
			userID = request.getParameter("userID");	// CWE-200 hychang
			user.setUserID(userID);
			if(request.getParameter("delete") != null && request.getParameter("delete").equals("true")) {
				delete = true;
			}
		}
		if(userID == null){	//로그인하지 않은 경우 회원 정보 수정 불가
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('로그인이 필요합니다.')");
			script.println("location.href = 'main.jsp'");
			script.println("</script>");
		}
		
		if(delete) {
			UserDAO userDAO = new UserDAO();
			int result = userDAO.delete(userID);
			if (result == -1) {
				PrintWriter script = response.getWriter();
				script.println("<script>");
				script.println("alert('오류가 발생했습니다.')");
				script.println("history.back()");
				script.println("</script>");
			} else {
				String cookieCheck = request.getHeader("Cookie");
				if(cookieCheck != null) {
					Cookie[] cookies = request.getCookies();
				    for(Cookie cookie : cookies) {
				    	cookie.setMaxAge(0);
				    	response.addCookie(cookie);
				    }
				}
				session.invalidate();
				PrintWriter script = response.getWriter();
				script.println("<script>");
				script.println("alert('회원 탈퇴 성공!')");
				script.println("location.href = 'main.jsp'");
				script.println("</script>");
			}
		}
		else if (user.getUserPassword() == null || user.getUserName() == null
			|| user.getUserGender() == null || user.getUserEmail() == null) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('입력이 안된 사항이 있습니다.')");
			script.println("history.back()");
			script.println("</script>");
		} else {
			UserDAO userDAO = new UserDAO();
			int result = userDAO.edit(user);
			if (result == -1) {
				PrintWriter script = response.getWriter();
				script.println("<script>");
				script.println("alert('오류가 발생했습니다.')");
				script.println("history.back()");
				script.println("</script>");
			} else {
				PrintWriter script = response.getWriter();
				script.println("<script>");
				script.println("alert('회원 정보 변경!')");
				script.println("location.href = 'main.jsp'");
				script.println("</script>");
			}
		}
	%>
</body>
</html>