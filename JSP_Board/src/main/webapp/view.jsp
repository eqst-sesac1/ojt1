<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="bbs.Bbs"%>
<%@ page import="bbs.BbsDAO"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.css">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		boolean admin = false;
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
			if(userID.equals("admin")){
				admin = true;
			}
		}
		String cookieCheck = request.getHeader("Cookie");
		if(cookieCheck != null) {
			Cookie[] cookies = request.getCookies();
		    for(Cookie cookie : cookies) {
		    	String key = cookie.getName();
		        String value = cookie.getValue();
		        // to - do
		    }
		}
		
		//게시판 관련 기본세팅
		int bbsID = 0;
		if(request.getParameter("bbsID") != null)
		{
			bbsID = Integer.parseInt(request.getParameter("bbsID"));	//게시판 글 번호조회
		}
		if(bbsID == 0)
		{
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('유효하지 않은 글입니다.')");
			script.println("location.href = 'bbs.jsp'");
			script.println("</script>");
		}
		Bbs bbs = new BbsDAO().getBbs(bbsID); //게시글 상세 조회해서 bbs에 저장
	%>
	
	
  <nav class="navbar navbar-default">
  	<div class="navbar-header">
  		<button type="button" class="navbar-toggle collapsed"
  			data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
  			aria-expanded="false">
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="main.jsp">JSP 게시판 웹 사이트</a>
  	</div>
  	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  		<ul class="nav navbar-nav">
  			<li><a href="main.jsp">메인</a></li>
  			<li class="active"><a href="bbs.jsp">게시판</a></li>
  			<%
  				if(admin) {
  			%>
  			<li><a href="management.jsp">관리</a></li>
  			<%
  				}
  			%>
  		</ul>
  		<%
  			if(userID == null) {	//로그인 하지 않은 상태
  		%>
  		<ul class="nav navbar-nav navbar-right">
			<!-- hychang fix
			<li class="dropdown">
				<a href="#" class="dropdown-toggle"
				   data-toggle="dropdown" role="button" aria-haspopup="true"
				   aria-expanded="false">접속하기<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li class="active"><a href="login.jsp">로그인</a></li>
					<li><a href="join.jsp">회원가입</a></li>
				</ul>
			</li>
			-->
			<li><a href="login.jsp">로그인</a></li>
			<li><a href="join.jsp">회원가입</a></li>
  		</ul>
  		<%
  			} else {	//로그인한 상태
  		%>
  		<ul class="nav navbar-nav navbar-right">
			<!-- hychang fix
			<li class="dropdown">
				<a href="#" class="dropdown-toggle"
				   data-toggle="dropdown" role="button" aria-haspopup="true"
				   aria-expanded="false">회원관리<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="logoutAction.jsp">로그아웃</a></li>
				</ul>
			</li>
			-->
			<li><a href="edit.jsp?userID=<%=userID%>"><%=userID%> 님</a></li>
			<li><a href="logoutAction.jsp">로그아웃</a></li>
  		</ul>
  		<%
  			}
  		%>
  			
  	</div>
  </nav>

	<div class="container">
		<div class="row">
			<table class="table table-striped" style="text-align:center; border : 1px solid #dddddd;">
			<thead>
				<tr>
					<th colspan="3" style="background-color: #eeeeee; text-align:center;">게시판 글보기 양식</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 20%">글제목</td>
					<td colspan= "2"><%= bbs.getBbsTitle().replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>") %></td>
				</tr>
				<tr>
					<td>작성자</td>
					<td colspan= "2"><%= bbs.getUserID()  %></td>
				</tr>
				<tr>
				    <td>파일</td>
				    <td colspan="2">
				        <%
				            String uploadPath = "upload/" + bbs.getBbsID();
				            String[] bbsFileList = bbs.getBbsFileList();
				            if (bbsFileList != null && bbsFileList.length > 0) {
				                for (String bbsFileName : bbsFileList) {
				                    String filePath = uploadPath + "/" + bbsFileName;
				        %>
				                    <a href="<%= filePath %>" download="<%= bbsFileName %>"><%= bbsFileName %></a><br>
				        <% 
				                }
				            } else {
				        %>
				                  업로드된 파일이 없습니다.
				        <%  
				            } 
				        %>
				    </td>
				</tr>
				<tr>
					<td>작성일자</td>
					<td colspan= "2"><%= bbs.getBbsDate().substring(0, 11) + bbs.getBbsDate().substring(11, 13) + " 시 " + bbs.getBbsDate().substring(14, 16) +" 분" %></td>
				</tr>
				<tr>
					<td>내용</td>
					<td colspan= "2" style="min-height: 200px; text-align: left;"><%= bbs.getBbsContent().replaceAll(" \" ", "&quot;").replaceAll("(?i)<script>", "script") %></td>
				</tr>
			</tbody>
		</table>
		<a href="bbs.jsp" class="btn btn-primary">목록</a>
		
		
		<%
			// 게시물 작성자 또는 관리자일 경우에만 수정과 삭제 버튼 보이게
			if(userID != null && (userID.equals(bbs.getUserID()) || userID.equals("admin"))) {
				
		%>
			<a href="update.jsp?bbsID=<%= bbsID %>" class="btn btn-primary">수정</a>
			<a onclick="return confirm('정말로 삭제하시겠습니까?');" href="deleteAction.jsp?bbsID=<%= bbsID %>" class="btn btn-primary">삭제</a>
		<%
			}
		%>
		
			
		</div>
	</div>





  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
</body>
</html>