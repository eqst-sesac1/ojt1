<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="user.UserDAO" %>
<%@ page import="user.User" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.css">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		boolean admin = false;
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
			if(userID.equals("admin")){
				admin = true;
			}
		}
		userID = request.getParameter("userID");	// CWE-200 hychang
		User user = new UserDAO().getUser(userID);
	%>
	<nav class="navbar navbar-default">
  	<div class="navbar-header">
  		<button type="button" class="navbar-toggle collapsed"
  			data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
  			aria-expanded="false">
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="main.jsp">JSP 게시판 웹 사이트</a>
  	</div>
  	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  		<ul class="nav navbar-nav">
  			<li><a href="main.jsp">메인</a></li>
  			<li><a href="bbs.jsp">게시판</a></li>
  			<%
  				if(admin) {
  				// CWE-200 hychang
  			%>
  			<li><a href="management.jsp">관리</a></li>
  			<%
  				}
  			%>
  		</ul>
		<ul class="nav navbar-nav navbar-right">
  			<!-- hychang fix
  			<li class="dropdown">
  				<a href="#" class="dropdown-toggle"
  					data-toggle="dropdown" role="button" aria-haspopup="true"
  					aria-expanded="false">회원관리<span class="caret"></span></a>
  				<ul class="dropdown-menu">
  					<li><a href="logoutAction.jsp">로그아웃</a></li>
  				</ul>
  			</li>
  			-->
  			<li class="active"><a href="edit.jsp?userID=<%=userID%>"><%=userID%> 님</a></li>
  			<li><a href="logoutAction.jsp">로그아웃</a></li>
  		</ul>
  	</div>
  </nav>
	<div class="container">
		<div class="col-lg-4"></div>
		<div class="col-lg-4">
			<div class="jumbotron" style="padding-top: 20px;">
				<form method="post" action="editAction.jsp?userID=<%=userID%>">
					<h3 style="text-align: center;">회원 정보 수정 화면</h3>
					<!-- hychang fix
					<div class="form-group">
						<input type="text" class="form-control" placeholder="아이디" name="userID" maxlength="20">
					</div>
					-->
					<div class="form-group">
						<input type="password" class="form-control" placeholder="비밀번호" name="userPassword" maxlength="20">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="이름" name="userName" value="<%=user.getUserName()%>" maxlength="20">
					</div>
					<div class="form-group" style="text-align: center;">
						<!-- hychang fix
						<input type="text" class="form-control" placeholder="성별" name="userGender" maxlength="20">
						-->
						<div class="btn-group" data-toggle="buttons">
							<input class="form-check-input" type="radio" name="userGender" value="남자"
							<% if(user.getUserGender().equals("남자")) {%>
								checked
							<% } %>
							>
							<label class="form-check-label" for="flexRadioDefault1">
								남자
							</label>
							<input class="form-check-input" type="radio" name="userGender" value="여자"
							<% if(user.getUserGender().equals("여자")) {%>
								checked
							<% } %>
							>
							<label class="form-check-label" for="flexRadioDefault2">
								여자
							</label>
						</div>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" placeholder="이메일" name="userEmail" value="<%=user.getUserEmail()%>" maxlength="20">
					</div>
					<input type="submit" class="btn btn-primary form-control" value="회원 정보 변경">
				</form>
				<p></p>
				<form method="post" action="editAction.jsp?userID=<%=userID%>&delete=true">
					<input type="submit" class="btn btn-danger form-control" value="회원 탈퇴">
				</form>
			</div>
		</div>
		<div class="col-lg-4"></div>
	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>