<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter"%>

<%@ page import="bbs.BbsDAO" %>
<%@ page import="bbs.Bbs" %>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.css">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		boolean admin = false;
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
			if(userID.equals("admin")){
				admin = true;
			}
		}
		
		int pageNumber = 1;		//기본페이지 
		if(request.getParameter("pageNumber") != null){
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));	//전달받은 파라미터는 int형으로 바꿔서 페이지번호 저장
		}
		String searchType = "";
		if(request.getParameter("searchType") != null){
			searchType = request.getParameter("searchType");
		}
		String searchText = "";
		if(request.getParameter("searchText") != null){
			searchText = request.getParameter("searchText");
		}
	%>
  <nav class="navbar navbar-default">
  	<div class="navbar-header">
  		<button type="button" class="navbar-toggle collapsed"
  			data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
  			aria-expanded="false">
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="main.jsp">JSP 게시판 웹 사이트</a>
  	</div>
  	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  		<ul class="nav navbar-nav">
  			<li><a href="main.jsp">메인</a></li>
  			<li class="active"><a href="bbs.jsp">게시판</a></li>
  			<%
  				if(admin) {
  			%>
  			<li><a href="management.jsp">관리</a></li>
  			<%
  				}
  			%>
  		</ul>
  		<%
  			if(userID == null) {	//로그인 하지 않은 상태
  		%>
  		<ul class="nav navbar-nav navbar-right">
  			<!-- hychang fix
  			<li class="dropdown">
  				<a href="#" class="dropdown-toggle"
  					data-toggle="dropdown" role="button" aria-haspopup="true"
  					aria-expanded="false">접속하기<span class="caret"></span></a>
  				<ul class="dropdown-menu">
  					<li class="active"><a href="login.jsp">로그인</a></li>
  					<li><a href="join.jsp">회원가입</a></li>
  				</ul>
  			</li>
  			-->
  			<li><a href="login.jsp">로그인</a></li>
			<li><a href="join.jsp">회원가입</a></li>
  		</ul>
  		<%
  			} else {	//로그인한 상태
  		%>
  		<ul class="nav navbar-nav navbar-right">
  			<!-- hychang fix
  			<li class="dropdown">
  				<a href="#" class="dropdown-toggle"
  					data-toggle="dropdown" role="button" aria-haspopup="true"
  					aria-expanded="false">회원관리<span class="caret"></span></a>
  				<ul class="dropdown-menu">
  					<li><a href="logoutAction.jsp">로그아웃</a></li>
  				</ul>
  			</li>
  			-->
  			
  			<li><a href="edit.jsp?userID=<%=userID%>"><%=userID%> 님</a></li>
  			<li><a href="logoutAction.jsp">로그아웃</a></li>
  		</ul>
  		<%
  			}
  		%>
  			
  	</div>
  </nav>
	<div class="container">
		<div class="row">
			<form method="get" name="search" action="bbs.jsp">
				<table class="pull-right">
					<tr>
						<td>
							<select class="form-control" name="searchType">
								<option value="bbsTitle">제목</option>
								<option value="bbsContent">내용</option>
								<option value="userID">작성자</option>
							</select>
						</td>
						<td>
							<input type="text" class="form-control" placeholder="검색어 입력" name="searchText" value="<%=searchText%>" maxlength="200">
						</td>
						<td>
							<button type="submit" class="btn btn-success">검색</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<p/>
	<div class="container">
		<div class="row">
			<table class="table table-striped" style="text-align:center; border : 1px solid #dddddd;">
				<thead>
					<tr>
						<th style="background-color: #eeeeee; text-align:center;">번호</th>
						<th style="background-color: #eeeeee; text-align:center;">제목</th>
						<th style="background-color: #eeeeee; text-align:center;">작성자</th>
						<th style="background-color: #eeeeee; text-align:center;">작성일</th> 
					</tr>
				</thead>
				<tbody>
					<%
						BbsDAO bbsDAO = new BbsDAO();
						ArrayList<Bbs> list = bbsDAO.getList(pageNumber, searchType, searchText); //현재의 페이지에서 게시글 List를 가져옴
						for(int  i = 0; i < list.size(); i++)
						{
					%>
					<tr>
						<!-- 게시글 리스트 출력 시 -->
						<td><%=list.get(i).getBbsID()%></td>
						<td><a href="view.jsp?bbsID=<%=list.get(i).getBbsID()%>"><%=list.get(i).getBbsTitle().replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>")%></a></td>
						<td><%= list.get(i).getUserID() %></td>
						<td><%= list.get(i).getBbsDate().substring(0, 11) + list.get(i).getBbsDate().substring(11, 13) + " 시 " + list.get(i).getBbsDate().substring(14, 16) +" 분" %></td>
					</tr>
					<%
						}
					%>

				</tbody>
			</table>

			<!-- 다음페이지가 만들어질 시 버튼 -->
			<%
				if (pageNumber != 1){
			%>
				<a href="bbs.jsp?pageNumber=<%=pageNumber - 1%>&searchType=<%= searchType %>&searchText=<%= searchText %>" class="btn btn-success btn-arrow-left">이전</a>
			<%
				} if(bbsDAO.nextPage(pageNumber + 1, searchType, searchText)){
			%>
				<a href="bbs.jsp?pageNumber=<%=pageNumber + 1%>&searchType=<%= searchType %>&searchText=<%= searchText %>" class="btn btn-success btn-arrow-left">다음</a>
			<%
				}
			%>

			<a href="write.jsp" class="btn btn-primary pull-right">글쓰기</a>
		</div>
	</div>





  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
</body>
</html>