package user;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;

import java.util.ArrayList;
import java.util.Base64;

public class UserDAO {

	private Connection conn;
	private PreparedStatement pstmt;
	private ResultSet rs;
	private java.sql.Statement stmt;
	
	private final String AESKEY = "[hychang@sk.com]"; 

	public UserDAO(){
		try {
			String dbURL = "jdbc:mysql://localhost:3306/bbs";
			String dbID = "root";
			String dbPassword = "root";
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(dbURL, dbID, dbPassword);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String login(String userID, String userPassword) {
		String SQL = "SELECT userPassword FROM user WHERE userID = ?";
		try {
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1,  userID);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				if (rs.getString(1).equals(hashString(userPassword))) {
					//String result = rs.getString("userID");	//userID컬럼 값을 DB에서 가져옴
					return userID; // 로그인 성공
				} else {
					return "password incorrect"; // 비밀번호 불일치
				}
			}
			return "id null"; // 아이디가 없음
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "db error"; // 데이터베이스 오류
	}
	
	public int join(User user) {
		String SQL = "INSERT INTO user VALUES (?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, user.getUserID());
			pstmt.setString(2, hashString(user.getUserPassword()));
			pstmt.setString(3, user.getUserName());
			pstmt.setString(4, user.getUserGender());
			pstmt.setString(5, encrypt(user.getUserEmail()));
			return pstmt.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 
	 * @param userID
	 * @return
	 */
	public User getUser(String userID) {
		User user = null;
		String SQL = "SELECT userName, userGender, userEmail FROM user WHERE userID = ?";
		try {
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, userID);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				user = new User();
				user.setUserName(rs.getString(1));
				user.setUserGender(rs.getString(2));
				user.setUserEmail(decrypt(rs.getString(3)));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	/**
	 * hychang
	 * @param user
	 * @return
	 */
	public int edit(User user) {
		String SQL = "UPDATE user SET userPassword = ?, userName = ?, userGender = ?, userEmail = ? WHERE userID = ?";
		try {
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, hashString(user.getUserPassword()));
			pstmt.setString(2, user.getUserName());
			pstmt.setString(3, user.getUserGender());
			pstmt.setString(4, encrypt(user.getUserEmail()));
			pstmt.setString(5, user.getUserID());
			return pstmt.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * hychang
	 * @param user
	 * @return
	 */
	public int delete(String userID) {
		String SQL = "DELETE FROM user WHERE userID = ?";
		try {
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, userID);
			return pstmt.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * hychang
	 * @param pageNumber
	 * @param searchType
	 * @param searchText
	 * @return All User data 
	 */
	public ArrayList<User> getList(int pageNumber, String searchType, String searchText) {
		String SQL = "SELECT userName, userID, userGender, userEmail FROM user WHERE ? Like ?";
		ArrayList<User> list = new ArrayList<User>();
		int index = 0;
		try {
			PreparedStatement pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, searchType);
			pstmt.setString(2, "%" + searchText + "%");
			rs = pstmt.executeQuery();
			while(rs.next()) {
				if (index >= (pageNumber - 1) * 10 && index < pageNumber * 10) {
					User user = new User();
					user.setUserName(rs.getString(1));
					user.setUserID(rs.getString(2));
					user.setUserGender(rs.getString(3));
					user.setUserEmail(decrypt(rs.getString(4)));
					list.add(user);
				} else if (index >= pageNumber * 10) {
					break;
				}
				++index;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;//게시글 리스트 반환
	}
	
	/**
	 * hychang
	 * @param pageNumber
	 * @param searchType
	 * @param searchText
	 * @param sortItem
	 * @param sortType
	 * @return 
	 */
	public boolean nextPage(int pageNumber, String searchType, String searchText) {//페이지 처리를 위한 함수
		String SQL = "SELECT * FROM user WHERE ? Like ?";
		int index = 0;
		try {
			PreparedStatement pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, searchType);
			pstmt.setString(2, "%" + searchText + "%");
			rs = pstmt.executeQuery();
			while(rs.next()) {
				if (index >= (pageNumber - 1) * 10) {
					return true;
				}
				++index;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

    public String hashString(String plainText) {
        try {
            StringBuilder hexString = new StringBuilder();
            for (byte b : MessageDigest.getInstance("MD5").digest(plainText.getBytes())) {
                String hex = Integer.toHexString(0xFF & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String encrypt(String plainText) {
        try {
        	SecretKeySpec secretKey = new SecretKeySpec(AESKEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String decrypt(String cipherText) {
        try {
        	SecretKeySpec secretKey = new SecretKeySpec(AESKEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(cipherText)));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
}