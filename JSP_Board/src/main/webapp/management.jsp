<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter"%>

<%@ page import="user.UserDAO" %>
<%@ page import="user.User" %>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.css">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		boolean admin = false;
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
			if(userID.equals("admin")){
				admin = true;
			}
		}
		if(!admin) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('관리자만 접근할 수 있습니다.')");
			script.println("history.back()");
			script.println("</script>");
		}
		
		int pageNumber = 1;		//기본페이지
		if(request.getParameter("pageNumber") != null){
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));	//전달받은 파라미터는 int형으로 바꿔서 페이지번호 저장
		}
		String searchType = "";
		if(request.getParameter("searchType") != null){
			searchType = request.getParameter("searchType");
		}
		String searchText = "";
		if(request.getParameter("searchText") != null){
			searchText = request.getParameter("searchText");
		}
	%>
  <nav class="navbar navbar-default">
  	<div class="navbar-header">
  		<button type="button" class="navbar-toggle collapsed"
  			data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
  			aria-expanded="false">
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="main.jsp">JSP 게시판 웹 사이트</a>
  	</div>
  	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  		<ul class="nav navbar-nav">
  			<li><a href="main.jsp">메인</a></li>
  			<li><a href="bbs.jsp">게시판</a></li>
  			<li class="active"><a href="management.jsp">관리</a></li>
  		</ul>
  		<ul class="nav navbar-nav navbar-right">
  			<!-- hychang fix
  			<li class="dropdown">
  				<a href="#" class="dropdown-toggle"
  					data-toggle="dropdown" role="button" aria-haspopup="true"
  					aria-expanded="false">회원관리<span class="caret"></span></a>
  				<ul class="dropdown-menu">
  					<li><a href="logoutAction.jsp">로그아웃</a></li>
  				</ul>
  			</li>
  			-->
  			<li><a href="edit.jsp"><%=userID%> 님</a></li>
  			<li><a href="logoutAction.jsp">로그아웃</a></li>
  		</ul>
  	</div>
  </nav>
	<div class="container">
		<div class="row">
			<form method="get" name="search" action="management.jsp">
				<table class="pull-right">
					<tr>
						<td>
							<select class="form-control" name="searchType">
								<option value="userName">이름</option>
								<option value="userID">아이디</option>
								<option value="userEmail">이메일</option>
							</select>
						</td>
						<td>
							<input type="text" class="form-control" placeholder="검색어 입력" name="searchText" value="<%=searchText%>" maxlength="200">
						</td>
						<td>
							<button type="submit" class="btn btn-success">검색</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<p/>
	<div class="container">
		<div class="row">
			<table class="table table-striped" style="text-align:center; border : 1px solid #dddddd;">
				<thead>
					<tr>
						<th style="background-color: #eeeeee; text-align:center;">이름</th>
						<th style="background-color: #eeeeee; text-align:center;">아이디</th>
						<th style="background-color: #eeeeee; text-align:center;">성별</th>
						<th style="background-color: #eeeeee; text-align:center;">이메일</th> 
					</tr>
				</thead>
				<tbody>
					<%
						UserDAO userDAO = new UserDAO();
						ArrayList<User> list = userDAO.getList(pageNumber, searchType, searchText); //현재의 페이지에서 게시글 List를 가져옴
						for(int  i = 0; i < list.size(); i++)
						{
					%>
					<tr>
						<!-- 게시글 리스트 출력 시 -->
						<td><%=list.get(i).getUserName()%></td>
						<td><%=list.get(i).getUserID()%></td>
						<td><%=list.get(i).getUserGender()%></td>
						<td><%=list.get(i).getUserEmail()%></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>

			<!-- 다음페이지가 만들어질 시 버튼 -->
			<%
				if (pageNumber != 1){
			%>
				<a href="management.jsp?pageNumber=<%=pageNumber - 1%>&searchType=<%= searchType %>&searchText=<%= searchText %>" class="btn btn-success btn-arrow-left">이전</a>
			<%
				} if(userDAO.nextPage(pageNumber + 1, searchType, searchText)){
			%>
				<a href="management.jsp?pageNumber=<%=pageNumber + 1%>&searchType=<%= searchType %>&searchText=<%= searchText %>" class="btn btn-success btn-arrow-left">다음</a>
			<%
				}
			%>
		</div>
	</div>





  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
</body>
</html>