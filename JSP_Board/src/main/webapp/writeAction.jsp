<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="bbs.Bbs" %>
<%@ page import="bbs.BbsDAO" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<% request.setCharacterEncoding("UTF-8"); %>
<!-- hychang fix
<jsp:useBean id="bbs" class="bbs.Bbs" scope="page" />
<jsp:setProperty name="bbs" property="bbsTitle" />
<jsp:setProperty name="bbs" property="bbsContent" />
-->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		
		//현재 세션 상태 확인
		String userID = null;
		if(session.getAttribute("userID") != null)
		{
			userID = (String) session.getAttribute("userID");  //세션의 userid 확인
		}
		
		if(userID == null){	//로그인 한 경우에만 글 쓰기 가능
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('로그인을 하세요.')");
			script.println("location.href = 'login.jsp'");
			script.println("</script>");
		}
		
		else
		{
			BbsDAO bbsDAO = new BbsDAO();
			bbs = new Bbs();
			bbs.setBbsID(bbsDAO.getNext());
			
			String uploadPath = request.getSession().getServletContext().getRealPath("/") + "upload/" + bbs.getBbsID();
			File uploadDir = new File(uploadPath);
		    if (!uploadDir.exists()) {
		        uploadDir.mkdirs();
		    }
		    
		    MultipartRequest multi = new MultipartRequest(
	            request,
	            uploadPath,
	            10 * 1024 * 1024,
	            "UTF-8"
		    );

		    bbs.setBbsTitle(multi.getParameter("bbsTitle"));
		    bbs.setBbsContent(multi.getParameter("bbsContent"));

		    ArrayList<String> fileList = new ArrayList<String>();
		    Enumeration files = multi.getFileNames();
		    while (files.hasMoreElements()) {
		        String filename = multi.getFilesystemName((String) files.nextElement());		        
		        if (filename != null) {
		            fileList.add(filename);
		        }
		    }
		    bbs.setBbsFileList(fileList.toArray(new String[0]));
			
		    if(bbs.getBbsTitle() == null || bbs.getBbsContent() == null ||
				bbs.getBbsTitle().equals("") || bbs.getBbsContent().equals(""))
		    {
				PrintWriter script = response.getWriter();
				script.println("<script>");
				script.println("alert('입력이 안 된 사항이 있습니다.')");
				script.println("history.back()");
				script.println("</script>");
			} 
			else {
				int result = bbsDAO.write(bbs.getBbsTitle(), userID, bbs.getBbsContent(), bbs.getBbsFileList());
				if (result == -1) { //데이터 베이스 오류
					PrintWriter script = response.getWriter();
					script.println("<script>");
					script.println("alert('글쓰기에 실패했습니다.')");
					script.println("history.back()");
					script.println("</script>");
				} else {	//글쓰기 성공
					PrintWriter script = response.getWriter();
					script.println("<script>");
					script.println("alert('글쓰기에 성공했습니다.')");
					script.println("location.href = 'bbs.jsp'");
					script.println("</script>");
				}
			}
		}
	%>


</body>
</html>